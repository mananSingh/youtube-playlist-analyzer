# Youtube-playlist-analyzer
Youtube Playlist Analyzer uses Youtube Data API to analyze any youtube playlist or channel, and display the related information graphically. (youtube-api-test-1170)

**Website link**: [http://youtube-playlist-analyzer.appspot.com/](http://youtube-playlist-analyzer.appspot.com/)

**Mirror website link**: [http://youtube-api-test-1170.appspot.com](http://youtube-api-test-1170.appspot.com)

### Description

YouTube Playlist Analyzer uses YouTube Data API to analyze any youtube playlist or channel, and display the related information graphically. It supports 3 modes of data representations (or views): 

Playlist View, Timeline View and Table View. Use this tool to find out things like follows:
- Total Duration of a playlist?
- Compare video durations in a playlist/channel.
- What is the trend of views, likes, dislikes or comments on videos of a channel/playlist?
- Details of each video item - title, channel title, duration, counts, publishing time, short description, video link.


### Screenshots

![screenshot 1 - main - form] (https://raw.githubusercontent.com/mananSingh/Youtube-playlist-analyzer/master/Screenshots/1mainform.png)

![screenshot 2 - playlistView - durations] (https://raw.githubusercontent.com/mananSingh/Youtube-playlist-analyzer/master/Screenshots/2playlistviewdefault.png)

![screenshot 3 - playlistView - composite view](https://raw.githubusercontent.com/mananSingh/Youtube-playlist-analyzer/master/Screenshots/3playlistviewcomposite.png)

![screenshot 4 - timelineView  - default] (https://raw.githubusercontent.com/mananSingh/Youtube-playlist-analyzer/master/Screenshots/4timelineviewdefault.png)

![screenshot 5 - timelineView  - Item Select] (https://raw.githubusercontent.com/mananSingh/Youtube-playlist-analyzer/master/Screenshots/5itemSelect.png)

![screenshot 6 - timelineView  - durations] (https://raw.githubusercontent.com/mananSingh/Youtube-playlist-analyzer/master/Screenshots/6timelineduration.png)

![screenshot 7 - table View  - default] (https://raw.githubusercontent.com/mananSingh/Youtube-playlist-analyzer/master/Screenshots/7tableview.png)
